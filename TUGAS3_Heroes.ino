/* Nama : Veronika Juninda
   NIM  : 21/474552/PA/20495 
   Prodi: ELINS 2021 */

const int BUTTON1 = 2; //declare BUTTON1 into pin 2
const int BUTTON2 = 4; //declare BUTTON2 into pin 4
const int LED = 13; //declare LED into pin 13
int BUTTONstate1; //set value of BUTTONstate1 as 0
int BUTTONstate2; //set value of BUTTONstate2 as 0

void setup()
{
  pinMode(BUTTON1, INPUT); //set BUTTON1 as INPUT
  pinMode(BUTTON2, INPUT); //set BUTTON2 as INPUT
  pinMode(LED, OUTPUT); //set LED as OUTPUT
}

void loop()
{
  BUTTONstate1 = digitalRead(BUTTON1); //BUTTONstate1 sama dengan pembacaan BUTTON1
  BUTTONstate2 = digitalRead(BUTTON2); //BUTTONstate2 sama dengan pembacaan BUTTON2
  if ((BUTTONstate1 == HIGH)&&(BUTTONstate2 == LOW)) //jika BUTTON1 ditekan, BUTTONstate1 bernilai HIGH
  {                           //jika BUTTON2 tidak ditekan, BUTTONstate2 bernilai LOW
    digitalWrite(LED, HIGH); //LED menyala
    delay(500); //instruksi digitalWrite(LED, HIGH) dijalankan selama 500 milisecond atau 0.5 second
    digitalWrite(LED, LOW); //LED mati
    delay(500); //instruksi digitalWrite(LED, LOW) dijalankan selama 500 milisecond atau 0.5 second
    digitalWrite(LED, HIGH); 
    delay(500); 
    digitalWrite(LED, LOW);
    delay(500); 
    digitalWrite(LED, HIGH); 
    delay(500); 
    digitalWrite(LED, LOW);
    delay(500);
  } 
  else if ((BUTTONstate1 == HIGH)&&(BUTTONstate2 == HIGH)); //jika BUTTON1 ditekan, BUTTONstate1 bernilai HIGH
  {                             //jika BUTTON2 ditekan, BUTTONstate2 bernilai HIGH
    digitalWrite(LED, LOW); //lampu LED akan mati
  }
}

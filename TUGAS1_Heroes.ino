/* Nama : Veronika Juninda
   NIM  : 21/474552/PA/20495 
   Prodi: ELINS 2021 */

#define pinmotor1 7
#define pinmotor2 6
#define pinmotor3 3
#define pinmotor4 5

void setup() {
  pinMode(pinmotor1, OUTPUT);
  pinMode(pinmotor2, OUTPUT);
  pinMode(pinmotor3, OUTPUT);
  pinMode(pinmotor4, OUTPUT);
}

void loop() { 
  digitalWrite(pinmotor1, LOW);
  analogWrite(pinmotor2, 255);
  analogWrite(pinmotor3, 255);
  analogWrite(pinmotor4, 0);
}

/* Nama : Veronika Juninda
   NIM  : 21/474552/PA/20495 
   Prodi: ELINS 2021 */

const int pTrig = 10;
const int pEcho = 9;
const int pMotor1 = 3;
const int pMotor2 = 4;

void setup()
{
  Serial.begin(9600);
  pinMode(pTrig, OUTPUT);
  pinMode(pEcho, INPUT);
  pinMode(pMotor1, OUTPUT);
  pinMode(pMotor2, OUTPUT);
}
long durasi = 0;
void loop()
{
  analogWrite(pMotor1, LOW);
  delay(1000);
  digitalWrite(pMotor2, 255);
  delay(1000);
  
  digitalWrite(pTrig, HIGH);
  delay(10); // Wait for 1000 millisecond(s)
  digitalWrite(pTrig, LOW);
  
  durasi = pulseIn(pEcho, HIGH);
  Serial.print("Durasi: ");
  Serial.print(durasi);
  
  Serial.print(", Jarak: ");
  Serial.println((durasi*0.034)/2);
  delay(1000);
}
